<?php
  $errorFields = Array();
  if(isset($_POST['sendmessage'])) {
    if((strlen($_POST['name']) > 0)&&(strlen($_POST['telno']) > 0)&&(strlen($_POST['subject']) > 0)&&(strlen($_POST['message']) > 0)) {
      if(ctype_alpha(str_replace(' ', '', $_POST['name']))) {
        if(ctype_digit(str_replace(' ', '', $_POST['telno']))) {
          if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            if(ctype_alnum(str_replace(' ', '', $_POST['subject']))) {
              $mailSent = "OK";
              require_once("includes/vendor/swiftmailer/swift_required.php");

              $subject = $_POST['subject'];
              $from = array($_POST['email'] => $_POST['name']);
              $to = array(
               'k@kvarley.co.uk'  => 'Kevin Varley'
              );

              $transport = Swift_SmtpTransport::newInstance('smtp.mandrillapp.com', 587);
              $transport->setUsername('12009374@stu.mmu.ac.uk');
              $transport->setPassword('m29fcEGnhImahJJ60n4X5g');
              $swift = Swift_Mailer::newInstance($transport);

              $message = new Swift_Message($subject);
              $message->setFrom($from);
              $message->setBody($_POST['message'], 'text/html');
              $message->setTo($to);

              if ($recipients = $swift->send($message, $failures))
              {
               $mailSent = true;
              } else {
               $mailSent = false;
               //print_r($failures);
              }
            }
            else {
              $errorFields[] = "subject";
            }
          }
          else {
            $errorFields[] = "email";
          }
        }
        else {
          $errorFields[] = "telno";
        }
      }
      else {
        $errorFields[] = "name";
      }
    }
    else {
      $errorFields[] = "message";
      $errorFields[] = "subject";
      $errorFields[] = "email";
      $errorFields[] = "telno";
      $errorFields[] = "name";
      $errorMessage = "ERROR: Please fill in all fields!";
    }
  }

  $pagename = "Contact";
  $pagedescription = "Contact";
  require_once('elements/header.php');
?>
  <div class="col-md-9">
    <form method="POST" class="form-horizontal">
      <fieldset>

      <!-- Form Name -->
      <legend>Contact Us</legend>

      <?php
          if(isset($mailSent)) {
            if($mailSent === true) {
        ?>
              <div class="alert alert-success">
                <span>Your message has been sent successfully, we will be in touch shortly. Thank you for your time.</span>
              </div>
        <?php
            }
            else {
        ?>
              <div class="alert alert-danger">
                <span>Oops! An error occured while sending your message, please try again.</span>
              </div>
        <?php
            }
          }
        ?>

        <?php
          if(isset($errorMessage)) {
        ?>
            <div class="alert alert-danger">
              <span><?=$errorMessage;?></span>
            </div>
        <?php
          }
        ?>

        <?php
          if(sizeof($errorFields)>0) {
        ?>
          <div data-alert class="alert alert-danger">
            <span>Some of your entered data was invalid. Please see the highlighted fields and correct the data.</span>
          </div>
        <?php
          }
        ?>

      <!-- Text input-->
      <div class="form-group <?php if(in_array("name", $errorFields)){echo "has-error";}?>">
        <label class="col-md-4 control-label" for="name">Name</label>  
        <div class="col-md-4">
        <input id="name" name="name" type="text" placeholder="John Smith" class="form-control input-md" required=""  <?php if(!isset($mailSent) && isset($_POST['name'])) { echo "value=\"$_POST[name]\""; }?>>
          
        </div>
      </div>

      <!-- Text input-->
      <div class="form-group <?php if(in_array("telno", $errorFields)){echo "has-error";}?>">
        <label class="col-md-4 control-label" for="telno">Phone Number</label>  
        <div class="col-md-4">
        <input id="telno" name="telno" type="text" placeholder="01612472000" class="form-control input-md"  <?php if(!isset($mailSent) && isset($_POST['telno'])) { echo "value=\"$_POST[telno]\""; }?>>
          
        </div>
      </div>

      <!-- Text input-->
      <div class="form-group <?php if(in_array("email", $errorFields)){echo "has-error";}?>">
        <label class="col-md-4 control-label" for="email">Email</label>  
        <div class="col-md-4">
        <input id="email" name="email" type="text" placeholder="johnsmith@example.com" class="form-control input-md" required=""  <?php if(!isset($mailSent) && isset($_POST['email'])) { echo "value=\"$_POST[email]\""; }?>>
          
        </div>
      </div>

      <!-- Text input-->
      <div class="form-group <?php if(in_array("subject", $errorFields)){echo "has-error";}?>">
        <label class="col-md-4 control-label" for="subject">Subject</label>  
        <div class="col-md-4">
        <input id="subject" name="subject" type="text" placeholder="Help me please!" class="form-control input-md" required=""  <?php if(!isset($mailSent) && isset($_POST['subject'])) { echo "value=\"$_POST[subject]\""; }?>>
          
        </div>
      </div>

      <!-- Textarea -->
      <div class="form-group <?php if(in_array("message", $errorFields)){echo "has-error";}?>">
        <label class="col-md-4 control-label" for="message">Message</label>
        <div class="col-md-4">                     
          <textarea class="form-control" id="message" name="message"> <?php if(!isset($mailSent) && isset($_POST['message'])) { echo $_POST['message']; }?></textarea>
        </div>
      </div>

      <!-- Button -->
      <div class="form-group">
        <label class="col-md-4 control-label" for="sendmessage"></label>
        <div class="col-md-4">
          <button id="sendmessage" name="sendmessage" class="btn btn-primary">Send Message</button>
        </div>
      </div>

      </fieldset>
      </form>
    </div>
    <div class="col-md-3">
      <h3>Contact MMU</h3>
      <p><span class="bold">Call:</span> 0161 247 2000</p>
      <p><span class="bold">Write to:</span> Manchester Metropolitan University, All Saints Building, All Saints, Manchester, M15 6BH</p>
      <p><span class="bold">Email:</span> <a href="mailto:studentservices@mmu.ac.uk">studentservices@mmu.ac.uk</a></p>
      <p><a href="http://goo.gl/maps/QXVwG"><img src="media/images/staticmap.png" width="200" height="200" style="border:1px solid #CECECE;" alt="View the location of MMU on Google Maps"></a></p>
    </div>
<?php
  require_once('elements/footer.php');
?>