<?php
  $pagename = "Home";
  $pagedescription = "Home";
  require_once('elements/header.php');
?>
  <div class="jumbotron">
    <h1>Welcome</h1>
    <p>We are team Ignite from Manchester Metropolitan University. We have created this website as part of our Developing Infocomms Project. From this we wanted to guide first year undergraduate students who use public transport. Our main focus is buses that travel to MMU’s All Saints campus.</p>
    <p>We have focused on stagecoach buses, as these are the main buses used by students. There is an interactive map included which shows the stagecoach routes around MMU. As well as a timetable showing bus numbers and their approximate arrival times.</p>
    <p><a href="bus-routes.php" class="btn btn-primary btn-lg" role="button">View bus routes</a></p>
  </div>

  <div class="row">
    <div class="col-md-3">
      <div class="thumbnail">
        <img src="media/images/bus-station-1.jpg" alt="Piccadilly bus station">
      </div>
    </div>
    <div class="col-md-3">
      <div class="thumbnail">
        <img src="media/images/bus-station-2.jpg" alt="Piccadilly bus station">
      </div>
    </div>
    <div class="col-md-3">
      <div class="thumbnail">
        <img src="media/images/bus-station-3.jpg" alt="Piccadilly bus station">
      </div>
    </div>
    <div class="col-md-3">
      <div class="thumbnail">
        <img src="media/images/bus-station-4.jpg" alt="Piccadilly bus station">
      </div>
    </div>
  </div>
<?php
  require_once('elements/footer.php');
?>