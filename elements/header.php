<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?=$pagedescription?>">
    <meta name="author" content="Team Ignite">
    <title><?php echo $pagename; ?> | Manchester Bus Routes Guide For Students</title>
    <link href="assets/vendor/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/vendor/css/bootstrap-switch.min.css" rel="stylesheet">
    <link href="assets/css/main.css" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body class="ignite">

    <div class="navbar navbar-inverse" role="navigation">
      <div class="container">
      <div class="logo-container">
        <a class="logo-link" href="index.php"><img src="assets/images/mmulogo.png" alt="Home"></a>
      </div>
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li <?php if($pagename==="Home"){echo "class=\"active\"";}?>><a href="index.php">Home</a></li>
            <li <?php if($pagename==="Bus Routes"){echo "class=\"active\"";}?>><a href="bus-routes.php">Bus Routes</a></li>
            <li <?php if($pagename==="Contact"){echo "class=\"active\"";}?>><a href="contact.php">Contact</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container">