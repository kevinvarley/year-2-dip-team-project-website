<?php
  $pagename = "Bus Routes";
  $pagedescription = "Bus Routes";
  require_once('elements/header.php');
?>
  <div class="row">
    <div class="col-md-12">
      <h1>Bus Routes</h1>
      <p> Stagecoach UK Bus is part of the Stagecoach Group and is one of the largest bus operators in the UK, operating express and local bus services across the country, as well as a comprehensive network of intercity operations under the megabus.com brand. They connect communities in over 100 towns and cities in the UK, running a fleet of around 8,100 buses and coaches that is one of the largest in the country.</p>
      <p>There are a number of services that run in Manchester but the most common services used by students are the services 42, 43, 142, 143, 15, x50 and 50. Majority of our university students will have used a stagecoach bus once in their life as it’s reliable, fairly cheap and stops right outside their preferred destination.</p>
      <p>The stagecoach buses depart and arrive at Piccadilly Gardens every 5-10 minutes or so, through early morning till late night.</p>
      <ul>
        <li><a href="http://www.tfgm.com/journey_planning/RouteMaps/42.pdf">View The 42 Bus Timetable (PDF)</a></li>
        <li><a href="http://www.tfgm.com/journey_planning/RouteMaps/43.pdf">View The 43 Bus Timetable (PDF)</a></li>
        <li><a href="http://www.tfgm.com/journey_planning/RouteMaps/142.pdf">View The 142 Bus Timetable (PDF)</a></li>
        <li><a href="http://www.tfgm.com/journey_planning/RouteMaps/143.pdf">View The 143 Bus Timetable (PDF)</a></li>
        <li><a href="http://www.tfgm.com/journey_planning/RouteMaps/15.pdf">View The 15 Bus Timetable (PDF)</a></li>
        <li><a href="http://www.tfgm.com/journey_planning/RouteMaps/50.pdf">View The 50 Bus Timetable (PDF)</a></li>
      </ul>
    </div>
  </div>

  <div class="row">
    <div class="col-md-10">
      <div id="map-canvas"></div>
    </div>
    <div class="col-md-2">
      <p>Legend</p>
      <p><button id="42" class="btn btn-default routetoggle" data-routenumber="42">Hide 42</button></p>
      <p><button id="43" class="btn btn-default routetoggle" data-routenumber="43">Hide 43</button></p>
      <p><button id="142" class="btn btn-default routetoggle" data-routenumber="142">Hide 142</button></p>
      <p><button id="143" class="btn btn-default routetoggle" data-routenumber="143">Hide 143</button></p>
      <p><button id="15" class="btn btn-default routetoggle" data-routenumber="15">Hide 15</button></p>
      <p><button id="50" class="btn btn-default routetoggle" data-routenumber="50">Hide 50</button></p>
    </div>
  </div>
<?php
  require_once('elements/footer.php');
?>