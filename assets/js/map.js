var map, path = [], service = new google.maps.DirectionsService(), poly, markers = [], mysvg, mylabel, editmode = false, setRegion, line, routes, polylines = [];

$(document).on("click", ".routetoggle", function() {
  var routeNumber = Number($(this).attr("data-routenumber"));
  if(polylines[routeNumber].getMap() === null) {
    polylines[routeNumber].setMap(map);
    $("#" + routeNumber).text("Hide " + routeNumber);
  }
  else {
    polylines[routeNumber].setMap(null);
    $("#" + routeNumber).text("Show " + routeNumber);
  }
});

function initialize() {
  $("#map-canvas").width($(".col-md-10").width());
  $("#map-canvas").height($(window).height()-200);
  var mapOptions = {
    zoom: 15,
    center: new google.maps.LatLng(53.470391,-2.238374),
    mapTypeId: google.maps.MapTypeId.MAP
  };
  map = new google.maps.Map(document.getElementById('map-canvas'),
      mapOptions);

  myRoute = "_|ynI~zeH?FiBX{@?FdCFz@f@fB|@vAdAfAj@HxDNb@CfAw@pAgA??~@q@dAKrAc@l@k@r@eAZy@DQ??J_@JcBDwA?sAUcDYaB??q@iDS}@IBQGMOEMk@t@??iBnC}BxE{@tAs@|@cA|@mAz@q@^]H[H??MB";

  window.routes = JSON.parse('{"42": ["ihleIdwsLvEvI`@`AlEdJvCxFl@fAnAlDHH|DqD|FwEzB{BjCaC~D}C~GeGbIkHvFqErOmNnJ_LvBwBzD_DjBeAzB_A|DiAlDuBlFaDzEuC|BeAtC_AhB}AvAcBhD_E|DkBtDsBxGkFhEsBtBw@lBmB|AmChA{@pAk@zC?pCf@jBb@hBRpDBbCFjBn@lAl@ jDhCxElEnBdBfBlC|DpHvDlG", "#FF0000"], "43": ["kqkeId_uLYmAo@cBq@oAeAoBa@y@a@}@QROb@a@bAc@|@cCvF`CzCt@XbA`CdBgCpB_BNM?@DEFEfEwD|FwEzB{BjCaC~D}C~GeGbIkHvFqErOmNnJ_LvBwBzD_DjBeAzB_A|DiAlDuBlFaDzEuC|BeAtC_AhB}AvAcBhD_E|DkBtDsBxGkFhEsBtBw@lBmB|AmChA{@pAk@zC?pCf@jBb@hBRpDBbCFjBn@lAl@jDhCxElEnBdBfBlC|DpHlDpF~CpKn@lBdLxIlKpKl`@x^zF~GdUx[nFpFrJlIrLpLdDfEbCrE~I`UzAzFfArDvA|B|LrF|BzArBnC|@tBl@vBJWJ{@BcEk@oGNcCNkBrBcAfBkAx@sAj@}Bb@{Ab@e@|AaA`Ae@tAa@xAi@f@YbARf@f@v@fDr@jEhCuCtJdM~DrDvCrBzAp@jBo@f@cBp@]r@j@j@`BlERnD?lDs@fDiAlD_@jFSn]iAfRn@vQ`FhHtBjEtAlB|@tJdHlCjBu@dH}@fMWhNBv[tH[nBnAhBzC`GhNjCwCvDkB~Bl@~@sE", "#0000FF"], "142": ["ihleIdwsLvEvI`@`AlEdJvCxFl@fAnAlDHH|DqD|FwEzB{BjCaC~D}C~GeGbIkHvFqErOmNnJ_LvBwBzD_DjBeAzB_A|DiAlDuBlFaDzEuC|BeAtC_AhB}AvAcBhD_E|DkBtDsBxGkFhEsBtBw@lBmB|AmChA{@pAk@zC?pCf@jBb@hBRpDBbCFjBn@lAl@jDhCxElEnBdBfBlC|DpHvDlG", "#FFFF00"], "143": ["ihleIdwsLvEvI`@`AlEdJvCxFl@fAnAlDHH|DqD|FwEzB{BjCaC~D}C~GeGbIkHvFqErOmNnJ_LvBwBzD_DjBeAzB_A|DiAlDuBlFaDzEuC|BeAtC_AhB}AvAcBhD_E|DkBtDsBxGkFhEsBtBw@lBmB|AmChA{@pAk@zC?pCf@jBb@hBRpDBbCFjBn@lAl@jDhCxElEnBdBfBlC|DpHrDvGpFtNvMrK|a@ja@dHvGtZna@fZtYpIrIfI|PzBlGxBrGlAxEhAbCdBnBxF|BjEzBnBbB|AjCpBlFhCrLzClBgD|FBsIMoB", "#00FF00"], "15": ["ihleIdwsLvEvI`@`AlEdJvCxFl@fAnAlDHH|DqD|FwEzB{BjCaC~D}C~GeGbIkHvFqErOmNnJ_LvBwBzD_DjBeAzB_A|DiArGsEXdROpNk@lRKnND`HXfEN`BMbBiBbGyAvEaB~KmEr]dArN`AhMnDrP", "#CB00F5"], "50": ["ixjeIpj_M??_AB??u@k@??k@uA??y@iB??YuC??i@aE??i@{D??e@wE??YuE??g@wD??_AcF??aAcC??_BmA??aAn@??_@|A??g@`F??o@~E??NtJ???fH??^rD??t@zB??nBfF??kDlC??{C???mCk@??iCq@??aLeC??mFuA??wE???qEL??f@e[??gGC??kLJ??kA`@??o@P??Wi@??EeA??^e@??`@yA??^kB??PiB??e@wB??k@g@??cFuE??eCgB??m@\??c@xB??_ArD??m@l@??e@S??QcB??LqA??F{A??_@c@??a@@??u@???_@M??Ye@??EmA??CaA??VeA??d@_A??`@{@??RcB??n@uA??b@mA??\aA??Zu@??QwA??lP_P??bEmG??bHoQ??xCuN??nAeI??dB}F??t@gP??WqL??BiL??G}J???eI??]eV??pGmG??hAmC??x@yC??`CkX??tAoJ??h@iE??jByF??|BqE??dDmI??vB_F??~D_G??rAqA??jBwB??fEeA??hCmB??pCsB??hByA??dCiD??`BoB??lAoB??~DvR??}FhF??}GnF??{DvD??qClC??}FrE??wCvB??oBpB??u@vA??q@fB??MlC??GvA??uEaA??U`@??OhA??UV??g@Q??aC{A??UW??", "#000000"]}');

  $.each(routes, function(i,v) {
    polylines[i] = new google.maps.Polyline({
      path: google.maps.geometry.encoding.decodePath
(v[0]),
      icons: [{
        icon: lineSymbol,
        offset: '100%'
      }],
      levels: decodeLevels(v[0]),
      strokeColor: v[1],
      map: map
    });
  });

  var lineSymbol = {
    path: google.maps.SymbolPath.CIRCLE,
    scale: 8,
    fillColor: 'red',
    fillOpacity: 1,
    strokeWeight: 0
  };

  var arrow = {
    path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
    fillColor: "red",
    fillOpacity: 0.8,
    scale: 5,
    strokeColor: "red",
    strokeWeight: 0,
  };

  var contentString = "<div><p>Individuals name and address and company info could go here.</p></div>";

  var infowindow = new google.maps.InfoWindow({
    content: contentString
  });

}

// Add a marker to the map and push to the array.
function addMarker(MarkerName, MarkerPath, MarkerLocation) {
  marker = new google.maps.Marker({
    position: MarkerLocation,
    map: map,
    title: MarkerName,
    draggable: true,
    icon: MarkerPath,
  });
  markers.push(marker);
}

// Sets the map on all markers in the array.
function setAllMap(map) {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
  }
}

// Removes the overlays from the map, but keeps them in the array.
function clearOverlays() {
  setAllMap(null);
}

// Shows any overlays currently in the array.
function showOverlays() {
  setAllMap(map);
}

// Deletes all markers in the array by removing references to them.
function deleteOverlays() {
  clearOverlays();
  markers = [];
}

function animateCircle() {
    var count = 0;
    offsetId = window.setInterval(function() {
      count = (count + 1) % 200;
      var icons = line.get('icons');
      icons[0].offset = (count / 2) + '%';
      line.set('icons', icons);
  }, 50);

}

function savePoly() {
  path = poly.getPath();
  encodeString = google.maps.geometry.encoding.encodePath(poly.getPath());
  console.log("Encoded poly=" + encodeString + ".");
}

function decodeLevels(encodedLevelsString) {
    var decodedLevels = [];
    for (var i = 0; i < encodedLevelsString.length; i++) {
      var level = encodedLevelsString.charCodeAt(i) - 63;
      decodedLevels.push(level);
    }
    return decodedLevels;
  }

function loadPoly(myRoute) {
  encodedLevels = myRoute;
  var decodedPath = google.maps.geometry.encoding.decodePath
(encodedLevels);
  var decodedLevels = decodeLevels(encodedLevels);

  setRegion = new google.maps.Polyline({
  path: decodedPath,
  levels: decodedLevels,
  strokeColor: "#0000FF",
  strokeOpacity: 1.0,
  strokeWeight: 5,
  map: map
});
}

google.maps.event.addDomListener(window, "resize", function() {
  $("#map-canvas").width($(".col-md-10").width());
  $("#map-canvas").height($(window).height()-200);
  var center = map.getCenter();
  google.maps.event.trigger(map, "resize");
  map.setCenter(center); 
});

google.maps.event.addDomListener(window, 'load', initialize);